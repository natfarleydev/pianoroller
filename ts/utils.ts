import WebMidi, { InputEventNoteoff } from "webmidi";
import { Note } from "./note";

const WHITE_NOTES = [0, 2, 4, 5, 7, 9, 11];
const BLACK_NOTES = [1, 3, 6, 8, 10];

/**
 * Predicate to ascertain if the given note is a black key
 * @param noteNumber MIDI number of the note
 */
export function isBlackNote(noteNumber: number): boolean {
  return BLACK_NOTES.includes(noteNumber % 12);
}

/**
 * Predicate to ascertain if the given note is a white key
 * @param noteNumber MIDI number of the note
 */
export function isWhiteNote(noteNumber: number): boolean {
  return WHITE_NOTES.includes(noteNumber % 12);
}

/**
 * Returns the number of ms a single beat lasts
 * @param bpm Beats per minute
 */
export function oneBeatInMsGivenBPM(bpm: number) {
  return 1 / (bpm / (60 * 1000));
}

/**
 * Plays a C major scale on all midi outputs.
 */
export function playSomeNotes() {
  const options = { duration: 1000, velocity: 0.5 };
  const notes = ["C4", "D4", "E4", "F4", "G4", "A4", "B4", "C5"];

  const bpm = 240;
  notes.forEach((v, i) => {
    const index = i;
    setTimeout(() => {
      WebMidi.outputs.forEach(o =>
        o.playNote(v, 1, { velocity: (index + 1) / 10, duration: 400 })
      );
    }, i * oneBeatInMsGivenBPM(bpm));
  });
}

/**
 * Gets the last note without a .noteoff value
 */
export function lastWithoutNoteoff(
  arr: Note[],
  noteoff: InputEventNoteoff
): Note | null {
  for (const note of arr) {
    if (
      note.noteoff === null &&
      note.noteon.note.number === noteoff.note.number
    ) {
      return note;
    }
  }

  // If for some reason all notes have a noteoff assigned, throw.
  throw new Error("Unable to find matching noteon for: " + noteoff.note.name);
}

export function midiVelocityToDb(velocity: number) {
  return 1 - Math.log10(1 / velocity);
}
