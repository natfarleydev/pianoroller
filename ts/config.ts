import { ITheme } from "./theme";

interface IConfig {
  /**
   * Color scheme as a function. Takes number [0, 1], returns hex color
   */
  blockColorScheme: (n: number) => string;

  /** Velocity of blocks in anvas pixels per millisecond */
  velocity: number;

  /** DOM element for the block color picker */
  blockColorPicker: any | null;

  /** DOM element for the background color picker */
  backgroundColorPicker: any | null;

  /**
   * Defines whether blocks are painted on top of the background or knock's out
   * the background with clearRect
   */
  blockOverlay: "knockout" | "overprint";

  /** Highest note displayed on the canvas */
  highestNote: number;

  /** Lowest note displayed on the canvas */
  lowestNote: number;

  /** Bottom of the region where blocks should be rendered */
  blockBottom: number | null;

  /** Top of the region where keyboard should be rendered */
  keyboardTop: number | null;

  /** Bottom of the region where keyboard should be rendered */
  keyboardBottom: number | null;

  /** User configurable theme (mostly colors) */
  theme: ITheme;
}

// null or 0 values should be initialised on page load
export const config: IConfig = {
  backgroundColorPicker: null,
  blockBottom: null,
  blockColorPicker: null,
  blockColorScheme: null,
  blockOverlay: "knockout",
  highestNote: 108,
  keyboardBottom: null,
  keyboardTop: null,
  lowestNote: 21,
  theme: {
    author: "Nathanael Farley",
    backgroundColor: "#000000bb",
    blockColor: "#FF1690",
    // Random unsplash image from 'https://unsplash.com/collections/256789/abstract-and-textures'
    cssBackground:
      "black url(https://source.unsplash.com/collection/256789/1920x1080) 100% 100%",
    experimental: {
      enableClouds: false
    },
    name: "Default theme"
  },
  velocity: 0.1
};

export default config;
