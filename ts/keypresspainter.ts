import { KeyPainter } from "./keypainter";
import { interpolateGreens } from "d3-scale-chromatic";
import { Note } from "./note";
import { midiVelocityToDb } from "./utils";

export class KeyPressPainter extends KeyPainter {
  public blackKeyColorFn: Function = interpolateGreens;
  public whiteKeyColorFn: Function = interpolateGreens;
  public note: Note;

  constructor(note: Note, ...args) {
    super(...args);
    this.note = note;
  }

  // We override the setter to ignore input
  // TODO do this a better way, making unintuitive behaviour is always a bad idea
  public get blackKeyColor() {
    return this.blackKeyColorFn(midiVelocityToDb(this.note.noteon.velocity));
  }
  public set blackKeyColor(val) {}

  public get whiteKeyColor() {
    return this.whiteKeyColorFn(midiVelocityToDb(this.note.noteon.velocity));
  }
  public set whiteKeyColor(val) {}
}
