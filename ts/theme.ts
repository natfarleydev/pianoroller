import config from "./config";
import store from "./store";

export interface ITheme {
  blockColor: string;
  backgroundColor: string;
  cssBackground: string;
  experimental: {
    enableClouds: boolean;
  };
  name: string;
  author: string;
}

/** Applies the current theme */
export function applyCurrentTheme(): void {
  console.log("[applyCurrentTheme] JSON from config:");
  console.log(JSON.stringify(config.theme, null, 2));
  store.requestRenderBackground = true;
  if (config.theme.cssBackground) {
    document.body.style.background = config.theme.cssBackground;
  }

  // Experimental cloud support
  const cloudAnimateInput: HTMLInputElement = document.querySelector(
    "input[name=cloud-animate]"
  );
  cloudAnimateInput.checked = config.theme.experimental.enableClouds;

  const elements: NodeListOf<HTMLElement> = document.querySelectorAll(".cloud");
  for (const el of elements) {
    el.style.display = config.theme.experimental.enableClouds
      ? "inline-block"
      : "none";
  }
}

export function loadTheme() {
  const files = (document.getElementById(
    "choose-theme-file"
  ) as HTMLInputElement).files;
  if (files.length <= 0) {
    alert("No files chosen!");
  }

  const fr: FileReader = new FileReader();

  fr.onload = e => {
    config.theme = JSON.parse(e.target.result);
    console.log("[loadTheme] JSON from file :");
    console.log(JSON.stringify(config.theme, null, 2));
    applyCurrentTheme();
  };

  fr.readAsText(files.item(0));
}
// export function loadTheme(json: string): void {
//   const newTheme = JSON.parse(json);
//   config.theme = newTheme;
//   applyCurrentTheme();
// }

export function stringifyTheme(): string {
  return JSON.stringify(config.theme, null, 2);
}

export function downloadTheme(): void {
  const dataStr = JSON.stringify(config.theme, null, 2);
  const dataUri =
    "data:application/json;charset=utf-8," + encodeURIComponent(dataStr);

  const exportFileDefaultName = "defaultTheme.json";
  const virtualLinkElement = document.createElement("a");
  virtualLinkElement.setAttribute("href", dataUri);
  virtualLinkElement.setAttribute("download", exportFileDefaultName);
  virtualLinkElement.click();
}
