import { InputEventNoteoff, InputEventNoteon } from "webmidi";
import config from "./config";
import store from "./store";

const LOWEST_NOTE = 21;
const HIGHEST_NOTE = 108;

export class Note {
  /** Note on event that defines the start of the note */
  public noteon: InputEventNoteon;

  /** Note off event that defines the end of the note */
  public noteoff: InputEventNoteoff | null;

  /**
   * Defines the duration of the note
   *
   * (cached from this.noteon and this.noteoff)
   */
  public duration: number;
  public rect: {
    x1: number;
    x2: number;
    y1: number;
    y2: number;
  };
  constructor(noteon: InputEventNoteon) {
    this.noteon = noteon;
    this.noteoff = null;
    this.duration = 0;
    this.rect = {
      x1: this.noteLeftEdge(),
      x2: this.noteRightEdge(),
      y1: this.blockBottom(),
      y2: this.blockBottom()
    };
  }

  public isNoteOffCanvas() {
    const canvas: HTMLCanvasElement = document.getElementsByTagName(
      "canvas"
    )[0];
    if (
      (this.rect.x1 > canvas.width && this.rect.x2 > canvas.width) ||
      (this.rect.x1 < 0 && this.rect.x2 < 0)
    ) {
      return true;
    }
    if (
      (this.rect.y1 > canvas.height && this.rect.y2 > canvas.height) ||
      (this.rect.y1 < 0 && this.rect.y2 < 0)
    ) {
      return true;
    }
    return false;
  }

  public blockBottom() {
    return (document.getElementsByTagName("canvas")[0] as HTMLCanvasElement)
      .height;
  }

  // TODO this probably isn't the best place to calculate canvasHeight, but I need
  // to in the constructor. Maybe they should be passed in the constructor.
  // These problems are why I don't do classes anymore.
  public canvasHeight(): number {
    return (document.getElementsByTagName("canvas")[0] as HTMLCanvasElement)
      .height;
  }

  public canvasWidth(): number {
    return (document.getElementsByTagName("canvas")[0] as HTMLCanvasElement)
      .width;
  }

  public noteLeftEdge() {
    return (
      ((this.noteon.note.number - LOWEST_NOTE) / (HIGHEST_NOTE - LOWEST_NOTE)) *
      this.canvasWidth()
    );
  }

  public noteRightEdge() {
    return (
      ((this.noteon.note.number + 1 - LOWEST_NOTE) /
        (HIGHEST_NOTE - LOWEST_NOTE)) *
      this.canvasWidth()
    );
  }

  public render(timestamp: number) {
    this.rect.y2 =
      this.blockBottom() -
      config.velocity * (timestamp - this.noteon.timestamp);
    if (this.noteoff) {
      this.rect.y1 =
        this.blockBottom() -
        config.velocity * (timestamp - this.noteoff.timestamp);
    }
  }

  public draw(ctx: CanvasRenderingContext2D) {
    ctx.fillStyle = config.blockColorPicker.color.rgbString;
    ctx.fillRect(
      this.rect.x1,
      this.rect.y1,
      this.rect.x2 - this.rect.x1,
      this.rect.y2 - this.rect.y1
    );
  }
}

/**
 * Calculates the x position of the left edge of a note block
 * @param noteNumber MIDI number of the note
 * @param canvas Canvas that will be rendered on
 * @param localConfig local copy of the config
 */
export function noteLeftEdge(
  noteNumber: number,
  canvas: HTMLCanvasElement = store.canvas,
  localConfig = config
): number {
  return (
    ((noteNumber - localConfig.lowestNote) /
      (localConfig.highestNote + 1 - localConfig.lowestNote)) *
    canvas.width
  );
}

/**
 * Calculates the x position of the right edge of a note block
 * @param noteNumber MIDI number of the note
 * @param canvas Canvas that will be rendered on
 * @param localConfig local copy of the config
 */
export function noteRightEdge(
  noteNumber: number,
  canvas: HTMLCanvasElement = store.canvas,
  localConfig = config
): number {
  return noteLeftEdge(noteNumber + 1, canvas, localConfig);
}

/**
 * Calculates the width of a note block
 * @param noteNumber MIDI number of the note
 * @param canvas Canvas that will be rendered on
 * @param localConfig local copy of the config
 */
export function noteWidth(
  noteNumber: number = 0,
  canvas: HTMLCanvasElement = store.canvas,
  localConfig = config
): number {
  return (
    noteRightEdge(noteNumber, canvas, localConfig) -
    noteLeftEdge(noteNumber, canvas, localConfig)
  );
}
