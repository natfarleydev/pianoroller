import { config } from "./config";
import { Note } from "./note";
import { midiVelocityToDb } from "./utils";

export function blockLeftEdge(
  noteNumber: number,
  canvasWidth: number,
  lowestNote: number,
  highestNote: number
): number {
  return (
    ((noteNumber - lowestNote) / (highestNote + 1 - lowestNote)) * canvasWidth
  );
}

export class BlockPainter {
  /** Reference to note this represents */
  public note: Note | null;

  /** Reference to canvas to draw on */
  public canvas: HTMLCanvasElement;

  /** Reference to context to draw on */
  public context: CanvasRenderingContext2D;

  /** Velocity of the block in pixels/ms */
  public velocity: number;

  /** Canvas y position to start growing the block */
  public blockBottom: number;

  /** Lowest note displayed on the canvas */
  public lowestNote: number;

  /** Highest note displayed on the canvas */
  public highestNote: number;

  /** Cache of the rectangle variables */
  public rect: {
    x: number;
    y: number;
    width: number;
    height: number;
  };

  /**
   *
   * @param note Reference to note this represents
   * @param canvas Reference to canvas to draw on
   * @param context Reference to context to draw on
   * @param velocity Velocity in pixels/ms
   * @param blockBottom Canvas y position to start growing the block
   * @param lowestNote The lowest note the canvas is set to display
   * @param highestNote The highest note the canvas is set to display
   */
  constructor(
    note: Note,
    canvas: HTMLCanvasElement,
    context: CanvasRenderingContext2D,
    velocity: number,
    blockBottom: number,
    lowestNote: number = config.lowestNote,
    highestNote: number = config.highestNote
  ) {
    this.canvas = canvas;
    this.context = context;
    this.velocity = velocity;
    this.blockBottom = blockBottom;
    this.lowestNote = lowestNote;
    this.highestNote = highestNote;

    this.initialize(note);
  }

  public isOutOfBounds(): boolean {
    return this.rect.y + this.rect.height < 0;
  }

  public initialize(note: Note): void {
    this.note = note;
    this.rect = {
      height: 0,
      width: Math.ceil(
        blockLeftEdge(1, this.canvas.width, this.lowestNote, this.highestNote) -
          blockLeftEdge(0, this.canvas.width, this.lowestNote, this.highestNote)
      ),
      x: Math.floor(
        blockLeftEdge(
          this.note.noteon.note.number,
          this.canvas.width,
          this.lowestNote,
          this.highestNote
        )
      ),
      y: this.blockBottom
    };
  }

  /**
   * Renders the values for the fillRect.
   *
   * This floors the values to give crisper lines.
   * @param timestamp Timestamp to render
   */
  public render(timestamp: number) {
    const y = Math.floor(
      this.blockBottom -
        this.velocity * (timestamp - this.note.noteon.timestamp)
    );

    this.rect.y = y;
    if (!this.note.noteoff) {
      this.rect.height = Math.floor(this.blockBottom - y);
    }
  }

  /**
   * Sets the fillStyle on the canvas blocks will be painted on
   */
  public setFillStyle() {
    // this.context.fillStyle = config.blockColorPicker.color.rgbaString;
    this.context.fillStyle = config.blockColorScheme
      ? config.blockColorScheme(midiVelocityToDb(this.note.noteon.velocity))
      : config.theme.blockColor;
  }

  /**
   * Draw the block
   *
   * @param setFillStyle In order to save calls to fillStyle, by default it is disabled.
   */
  public draw(setFillStyle: boolean = false) {
    if (setFillStyle) {
      this.setFillStyle();
    }
    if (config.blockOverlay === "knockout") {
      this.context.clearRect(
        this.rect.x,
        this.rect.y,
        this.rect.width,
        this.rect.height
      );
    }
    this.context.fillRect(
      this.rect.x,
      this.rect.y,
      this.rect.width,
      this.rect.height
    );
  }
}
