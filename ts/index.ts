import * as d3ScaleChromatic from "d3-scale-chromatic";
import WebMidi, { InputEventNoteoff, InputEventNoteon } from "webmidi";

import { BlockPainter } from "./blockpainter";
import config from "./config";
import { KeyPainter } from "./keypainter";
import { KeyPressPainter } from "./keypresspainter";
import { Note, noteLeftEdge } from "./note";
import store from "./store";
import { isBlackNote, lastWithoutNoteoff, playSomeNotes } from "./utils";

import iro from "@jaames/iro";
import iroTransparencyPlugin from "iro-transparency-plugin";
import { applyCurrentTheme, downloadTheme, loadTheme } from "./theme";

iro.use(iroTransparencyPlugin);

// GAMELOOP

/**
 * Renders everything that needs a calculation before drawing
 */
function update(timestamp: number) {
  // Update the state of the world for the elapsed time since last render

  for (const blockPainter of store.blockPainters) {
    blockPainter.render(timestamp);
  }

  // Filter out resources that are no longer needed.
  //
  // TODO consider whether a pool would be more appropriate rather than leaving
  // these objects to be garbage collected
  //
  // TODO, cont. In fact, notes no longer need to be stored after recieving a
  // .noteoff and being passed to blockPainter. They could be cleanly destruted
  // on onNoteOff without looping even more here. But this would make extending
  // the code more difficult later if we want effects based on number of notes
  // on screen or other visualisations.
  const outOfBoundsNotes = store.blockPainters
    .filter(v => v.isOutOfBounds())
    .map(v => v.note);

  store.blockPainters = store.blockPainters.filter(v => !v.isOutOfBounds());

  // Filter notes that are out of bounds
  if (outOfBoundsNotes.length > 0) {
    store.notes = store.notes.filter(n => !outOfBoundsNotes.includes(n));
  }
}

/**
 * Draws the state of the visualisation
 */
function draw() {
  // If there are notes to be displayed the background color picker has been defined.
  if (
    (store.requestRenderBackground || store.notes.length > 0) &&
    config.theme.backgroundColor
  ) {
    store.requestRenderBackground = false;
    store.ctx.fillStyle = config.theme.backgroundColor;
    store.ctx.clearRect(0, 0, store.canvas.width, config.blockBottom);
    store.ctx.fillRect(0, 0, store.canvas.width, config.blockBottom);
  }

  if (store.blockPainters[0] && !config.blockColorScheme) {
    store.blockPainters[0].setFillStyle();
  }
  for (const blockPainter of store.blockPainters) {
    blockPainter.draw(config.blockColorScheme ? true : false);
  }

  const keyPaintersWhite = Object.keys(store.keyPainters)
    .map(k => store.keyPainters[k])
    .filter(v => v.shouldDraw && !v.isBlackNote());
  const keyPaintersBlack = Object.keys(store.keyPainters)
    .map(k => store.keyPainters[k])
    .filter(v => v.shouldDraw && v.isBlackNote());

  const keyPressPaintersWhite = Object.keys(store.keyPressPainters)
    .map(k => store.keyPressPainters[k])
    .filter(v => v.shouldDraw && !v.isBlackNote());
  const keyPressPaintersBlack = Object.keys(store.keyPressPainters)
    .map(k => store.keyPressPainters[k])
    .filter(v => v.shouldDraw && v.isBlackNote());

  // At the moment, keyPainter.shouldDraw is what manages the redraw.
  for (const keyPainter of keyPaintersWhite) {
    keyPainter.draw();
  }

  for (const keyPainter of keyPressPaintersWhite) {
    keyPainter.draw();
  }

  for (const keyPainter of keyPaintersBlack) {
    keyPainter.draw();
  }

  for (const keyPainter of keyPressPaintersBlack) {
    keyPainter.draw();
  }
}

function loop(timestamp: number): void {
  update(timestamp);
  draw();

  // lastRender = timestamp;
  window.requestAnimationFrame(loop);
}
// var lastRender = 0;
window.requestAnimationFrame(loop);

// END OF GAMELOOP

/**
 * Draws black notes either side of a white note if needed.
 *
 * Useful when drawing of a white note/pressed white note will block notes
 * either side of it
 * @param note Number representing MIDI note
 */
function drawBlackNotesEitherSideIfNeeded(note: number) {
  const leftBlackNoteKeyPainter = store.keyPainters[note - 1];
  // If it's a black note, you should always redraw
  leftBlackNoteKeyPainter.shouldDraw = leftBlackNoteKeyPainter.isBlackNote();

  const leftBlackNoteKeyPressPainter = store.keyPressPainters[note - 1];
  // If the black note is pressed (i.e. if a KeyPressPainter exists for this
  // note), they KeyPressPainter should redraw
  if (leftBlackNoteKeyPressPainter) {
    leftBlackNoteKeyPressPainter.shouldDraw = leftBlackNoteKeyPressPainter.isBlackNote();
  }

  const rightBlackNoteKeyPainter = store.keyPainters[note + 1];
  // If it's a black note, you should always redraw
  rightBlackNoteKeyPainter.shouldDraw = rightBlackNoteKeyPainter.isBlackNote();

  const rightBlackNoteKeyPressPainter = store.keyPressPainters[note - 1];
  // If the black note is pressed (i.e. if a KeyPressPainter exists for this
  // note), they KeyPressPainter should redraw
  if (rightBlackNoteKeyPressPainter) {
    rightBlackNoteKeyPressPainter.shouldDraw = rightBlackNoteKeyPressPainter.isBlackNote();
  }
}

/**
 * Function to do stuff on midi note on
 * @param {object} e Midi event
 */
function onNoteOn(e: InputEventNoteon) {
  const newNote = new Note(e);
  store.notes.push(newNote);
  store.keyPressPainters[e.note.number] = new KeyPressPainter(
    newNote,
    newNote.noteon.note.number,
    store.canvas,
    store.ctx
  );

  if (!isBlackNote(e.note.number)) {
    drawBlackNotesEitherSideIfNeeded(e.note.number);
  }

  store.blockPainters.push(
    new BlockPainter(
      newNote,
      store.canvas,
      store.ctx,
      config.velocity,
      config.blockBottom
    )
  );
}

/**
 * Function to do stuff on midi note off
 */
function onNoteOff(e: InputEventNoteoff) {
  const currentNote = lastWithoutNoteoff(store.notes, e);

  currentNote.noteoff = e;
  currentNote.duration = e.timestamp - currentNote.noteon.timestamp;

  // Remove the KeyPressPainter for this note
  delete store.keyPressPainters[e.note.number];

  // Mark the unpressed KeyPainter as should be drawn
  store.keyPainters[e.note.number].shouldDraw = true;

  if (!isBlackNote(e.note.number)) {
    drawBlackNotesEitherSideIfNeeded(e.note.number);
  }

  currentNote.render(performance.now());
}

window.onload = (): void => {
  setTimeout(applyCurrentTheme, 100);
  // set up the canvas
  store.canvas = document.getElementsByTagName("canvas")[0];
  store.ctx = store.canvas.getContext("2d");

  // hidpi support does not work and I do not have a test device.
  //
  // TODO test this when I have a test device
  // // Enable hidpi support as per:
  // // https://developer.mozilla.org/en-US/docs/Web/API/Window/devicePixelRatio#Example_1_Correcting_resolution_in_a_%3Ccanvas%3E
  // //
  // // This means that fonts will print at the correct size
  // store.canvas.style.width = window.innerWidth + "px";
  // store.canvas.style.height = window.innerHeight + "px";
  // store.canvas.width = window.innerWidth * window.devicePixelRatio;
  // store.canvas.height = window.innerHeight * window.devicePixelRatio;

  // store.ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
  // // End of hidpi scaling

  store.canvas.style.width = window.innerWidth + "px";
  store.canvas.style.height = window.innerHeight + "px";
  store.canvas.width = window.innerWidth;
  store.canvas.height = window.innerHeight;

  // Set up the iro color pickers
  const colorPickerConfig = {
    width: 150
  };

  config.blockColorPicker = new iro.ColorPicker("#block-color-picker", {
    ...colorPickerConfig,
    color: config.theme.blockColor,
    transparency: true
  });

  config.blockColorPicker.on("color:change", color => {
    config.theme.blockColor = color.hex8String;
  });

  config.backgroundColorPicker = new iro.ColorPicker(
    "#background-color-picker",
    {
      ...colorPickerConfig,
      color: config.theme.backgroundColor,
      transparency: true
    }
  );

  config.backgroundColorPicker.on("color:change", color => {
    config.theme.backgroundColor = color.hex8String;
  });

  // Set up d3 scheme picker
  const dropdown = document.getElementById("block-color-scheme");
  let dropdownInnerHTML = "";
  for (const key of Object.keys(d3ScaleChromatic)) {
    if (key.includes("interpolate")) {
      dropdownInnerHTML += `<option value="${key}">${key}</option>\n`;
    }
  }
  dropdown.innerHTML = dropdownInnerHTML;

  dropdown.onchange = e => {
    config.blockColorScheme =
      d3ScaleChromatic[(e.target as HTMLSelectElement).value];
  };

  config.blockBottom = Math.floor((6 * store.canvas.height) / 7);
  config.keyboardTop = config.blockBottom;
  config.keyboardBottom = store.canvas.height;

  for (let i = config.lowestNote; i <= config.highestNote; ++i) {
    // Make arrays of which note numbers are visible for iterating over later.
    if (isBlackNote(i)) {
      store.visibleNotesBlack.push(i);
    } else {
      store.visibleNotesWhite.push(i);
    }

    // Make all the keypainters
    store.keyPainters[i] = new KeyPainter(i, store.canvas, store.ctx);
  }

  document.getElementById("play-some-notes").onclick = playSomeNotes;
  document.getElementById("load-theme-file").onclick = loadTheme;
  document.getElementById("download-theme").onclick = downloadTheme;
  document.getElementById("cloud-animate").onchange = event => {
    // TODO make this an external function in experimental/clouds.ts or clouds.ts
    config.theme.experimental.enableClouds = (event.target as HTMLInputElement).checked;
    applyCurrentTheme();
  };

  /* Set the width of the side navigation to 250px */
  document.getElementById("reveal-nav-button").onclick = event => {
    // document.getElementById("mySidenav").style.width = "250px";

    document.getElementById("mySidenav").classList.toggle("open");
    document.getElementById("reveal-nav-button").classList.toggle("open");
  };

  WebMidi.enable(err => {
    if (err) {
      // TODO tell the user in a proper way.
      console.error("WebMidi could not be enabled.", err);
    } else {
      const warmStart = Boolean(window.localStorage.getItem("warmStart"));

      WebMidi.inputs.forEach(i => i.addListener("noteon", "all", onNoteOn));
      WebMidi.inputs.forEach(i => i.addListener("noteoff", "all", onNoteOff));

      // If a warm start has *not* been requested, play something.
      if (!warmStart) {
        playSomeNotes();
        setTimeout(() => {
          document
            .getElementById("splash-rectangle")
            .classList.add("hidden-animation");
          document
            .getElementById("splash-rectangle")
            .classList.remove("visible-animation");
        }, 2000);
      }
    }
  });
};

// TODO do this the right way
(window as any).toggleColorPickers = () => {
  const colorPickers = document.getElementById("color-pickers");
  if (colorPickers.style.visibility === "hidden") {
    colorPickers.style.visibility = "visible";
    colorPickers.style.display = "block";
  } else {
    colorPickers.style.visibility = "hidden";
    colorPickers.style.display = "none";
  }
};
