import { fromMidi } from "tonal-note";

import config from "./config";
import { noteLeftEdge, noteWidth } from "./note";

const WHITE_NOTES = [0, 2, 4, 5, 7, 9, 11];
const BLACK_NOTES = [1, 3, 6, 8, 10];

export class KeyPainter {
  /** The midi note that represents our note */
  public noteNumber: number;

  /** Lowest note on the canvas */
  public lowestNote: number;

  /** Highest note displayed on the canvas */
  public highestNote: number;

  /** Top y cordinate of keyboard */
  public keyboardTop: number;

  /** Bottom y cordinate of keyboard */
  public keyboardBottom: number;

  /** Canvas to print on */
  public canvas: HTMLCanvasElement;

  /** Context to print on */
  public ctx: CanvasRenderingContext2D;

  public whiteKeyColor: string = "white";
  public whiteKeyTextColor: string = "black";

  public blackKeyColor: string = "black";
  public blackKeyTextColor: string = "white";

  /**
   * On true, a redraw should be done.
   *
   * This is set to false when .draw() is called and .rectArgs has not changed since last draw
   */
  public shouldDraw: boolean = true;

  // private lastRect: {
  //   x: number;
  //   y: number;
  //   width: number;
  //   height: number;
  // };

  /** Arguments to pass to fillRect that forms the key (for caching) */
  private rectArgs: {
    x: number;
    y: number;
    width: number;
    height: number;
  };

  constructor(
    noteNumber: number,
    canvas: HTMLCanvasElement,
    ctx: CanvasRenderingContext2D,
    lowestNote: number = config.lowestNote,
    highestNote: number = config.highestNote,
    keyboardTop: number = config.keyboardTop,
    keyboardBottom: number = config.keyboardBottom
  ) {
    this.canvas = canvas;
    this.ctx = ctx;
    this.noteNumber = noteNumber;
    this.lowestNote = lowestNote;
    this.highestNote = highestNote;
    this.keyboardBottom = keyboardBottom;
    this.keyboardTop = keyboardTop;

    // Set this.rectArgs
    this.render();
  }

  /**
   * Renders the note (black or white)
   */
  public render(): void {
    const lastRect = { ...this.rectArgs };

    if (this.isBlackNote()) {
      this.rectArgs = {
        height: this.keyboardBottom - this.keyboardTop,
        width: noteWidth(this.noteNumber),
        x: noteLeftEdge(this.noteNumber),
        y: this.keyboardTop
      };
    } else {
      this.rectArgs = {
        height: this.keyboardBottom - this.keyboardTop,
        width: Math.floor(1 + (12 * noteWidth(this.noteNumber)) / 7),
        x: Math.floor(
          noteLeftEdge(this.noteNumber - (this.noteNumber % 12)) +
            (WHITE_NOTES.indexOf(this.noteNumber % 12) * (noteWidth() * 12)) / 7
        ),
        y: this.keyboardTop
      };
    }

    this.shouldDraw =
      JSON.stringify(lastRect) !== JSON.stringify(this.rectArgs);
  }

  public draw(): void {
    console.log(
      "Drawing " + this.noteName() + " unpressed at " + performance.now()
    );
    if (this.isBlackNote()) {
      this.ctx.fillStyle = this.blackKeyColor;
      this.ctx.fillRect(
        this.rectArgs.x,
        this.rectArgs.y,
        this.rectArgs.width,
        this.rectArgs.height / 2
      );
    } else {
      this.ctx.fillStyle = this.whiteKeyColor;
      this.ctx.fillRect(
        this.rectArgs.x,
        this.rectArgs.y,
        this.rectArgs.width,
        this.rectArgs.height
      );
    }

    // Set shouldDraw to false, .render() will change it to true if .rectArgs
    // changes, or it can be changed externally to flag the key should be
    // redrawn
    this.shouldDraw = false;
  }

  /**
   * Returns the note's name
   */
  public noteName(): string {
    return fromMidi(this.noteNumber);
  }

  /**
   * Returns whether the current note is a black key
   */
  public isBlackNote(): boolean {
    return BLACK_NOTES.includes(this.noteNumber % 12);
  }
}
