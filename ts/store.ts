import { BlockPainter } from "./blockpainter";
import { KeyPainter } from "./keypainter";
import { KeyPressPainter } from "./keypresspainter";
import { Note } from "./note";

class Store {
  /** Optional request to render background canvas */
  public requestRenderBackground: boolean = false;

  public canvas?: HTMLCanvasElement;
  public ctx?: CanvasRenderingContext2D;
  public keyPainters: { [s: string]: KeyPainter } = {};

  /** Visible white notes by number */
  public visibleNotesWhite: number[] = [];

  /** Visible black notes by number */
  public visibleNotesBlack: number[] = [];

  // public keyPaintersWhite: KeyPainter[] = [];
  // public keyPaintersBlack: KeyPainter[] = [];
  public keyPressPainters: { [s: string]: KeyPressPainter } = {};
  // public keyPressPaintersWhite: KeyPressPainter[] = [];
  // public keyPressPaintersBlack: KeyPressPainter[] = [];
  public blockPainters: BlockPainter[] = [];
  public notes: Note[] = [];
}

export default new Store();
