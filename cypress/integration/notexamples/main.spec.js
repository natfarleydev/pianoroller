/// <reference types="Cypress" />
import WebMidi from "webmidi";
import webmidi from "webmidi";

URL = "http://localhost:1234/";

function warmStart() {
  cy.window().then(win => {
    win.localStorage.setItem("warmStart", true);
  });
  cy.visit(URL);
  cy.window().should(win => {
    // If the outputs are empty, then we can't play them!
    const midiOutput = win.WebMidi.getOutputByName("Midi Through Port-0");
    expect(midiOutput).to.not.be.undefined;
    win.midiOutput = midiOutput;
    // expect(
    //   win.WebMidi.outputs,
    //   "Cypress has been unable to connect to any midi devices, the tests cannot proceed without them!"
    // ).to.not.be.empty;
  });
}

describe("Page loads", () => {
  beforeEach(warmStart);

  it("Loads the page and includes canvas.", () => {
    cy.get("canvas");
  });

  it("Should include noteArrays", () => {
    cy.window().then(win => {
      expect(win.noteArrays).to.be.an("object");
    });
  });
});

describe("noteArrays manipulation", () => {
  beforeEach(warmStart);

  it("Should have a single noteon event in", () => {
    const noteVal = 72;
    cy.window().then(win => {
      win.midiOutput.playNote(noteVal, 1);
      expect(win.noteArrays[noteVal]).to.be.an("array");
    });

    // Wait a moment for the event to tick over
    cy.wait(1);

    cy.window().then(win => {
      expect(win.noteArrays[noteVal][0]).to.be.an.object;
      expect(win.noteArrays[noteVal][0]).to.have.property("noteon");
    });
  });

  it("Should have a single noteon and single noteoff in", () => {
    const noteVal = 72;
    cy.window().then(win => {
      win.midiOutput.playNote(noteVal, 1, { duration: 1 });
      expect(win.noteArrays[noteVal]).to.be.an("array");
    });

    // Wait a moment for the event to tick over
    cy.wait(10);

    cy.window().then(win => {
      expect(win.noteArrays[noteVal][0]).to.be.an.object;
      expect(win.noteArrays[noteVal][0]).to.have.property("noteon");
      expect(win.noteArrays[noteVal][0]).to.have.property("noteoff");
      expect(win.noteArrays[noteVal][0])
        .to.have.property("duration")
        .to.be.closeTo(1, 5);
    });
  });
});

// describe("WebMidi works with Cypress?", () => {
//   beforeEach(warmStart);

//   it("Tries to use a midi input with cypress", () => {
//     // cy.get("@midiOutput").then(output => output.playNote("C4", 1));
//     cy.window().then(win => {
//       // win.midiOutput.playNote("C4");
//       win.midiOutput.playNote("C4", 1);
//     });
//     // Wait for at least one frame to pass
//     cy.wait(200);
//     cy.window().then(win => {
//       expect(Object.values(win.growingBlocks)).to.have.length.of.at.least(1);
//     });
//   });
// });

// describe("Rectangle creation", () => {
//   beforeEach(warmStart);

//   it("Midi input creates rect elements", () => {
//     cy.window().then(win => {
//       win.midiOutput.playNote("C4", 1);
//     });

//     cy.get("svg").children("rect");
//   });

//   it("Single quick midi input creates rect", () => {
//     cy.window().then(win => {
//       win.midiOutput.playNote("C4", 1, { duration: 1 });
//     });

//     cy.get("svg").children("rect");k
//   });

//   it("Single quick midi input grows and then moves", () => {
//     cy.window().then(win => {
//       win.midiOutput.playNote("C4", 1);
//       expect(
//         Object.values(win.growingBlocks),
//         "growingBlocks after noteOn"
//       ).to.have.length(1);
//       win.midiOutput.stopNote("C4", 1);
//       expect(Object.values(win.growingBlocks), "growingBlocks").to.have.length(
//         0
//       );
//       expect(win.movingBlocks).to.have.length.equal(1);
//     });
//   });

//   //   it("Midi inputs in quick succession do not crash ", () => {
//   //     cy.window().then(win => {
//   //       const numberOfNotes = 2;
//   //       for (let i in Array(numberOfNotes)) {
//   //         win.midiOutput.playNote("C4", 1, { duration: 1 });
//   //       }
//   //       expect(Object.values(win.growingBlocks)).to.have.length(0);
//   //       expect(win.movingBlocks).to.have.length(numberOfNotes);
//   //     });

//   //     cy.get("svg").children("rect");
//   //   });
// });

// describe("Warm start behaviour", () => {
//   beforeEach(warmStart);

//   it("Should recognise a warm start has been requested", () => {
//     cy.window().then(win =>
//       expect(Boolean(win.localStorage.getItem("warmStart"))).to.be.equal(true)
//     );
//   });

//   it("Should not have any rect's present on the page", () => {
//     cy.get("rect").should("not.exist");
//   });
// });

// describe("Rectangles grow over time", () => {
//   beforeEach(warmStart);

//   it("Tests that the rectangle has grown ", () => {
//     cy.window().then(win => {
//       win.midiOutput.playNote("C4", 1, { duration: 1000 });
//     });

//     cy.get("svg")
//       .children("rect")
//       .should("have.attr", "height")
//       .should("be.gt", 0);
//   });

//   it("Should have non-zero rect height for short note", () => {
//     cy.window().then(win => {
//       // Play a note so fast, there is not a frame draw between noteOn and
//       // noteOff
//       win.midiOutput.playNote("C4", 1, { duration: 1 });
//     });

//     cy.get("svg")
//       .children("rect")
//       .should("have.attr", "height")
//       .should("be.gt", 0);
//   });
// });
