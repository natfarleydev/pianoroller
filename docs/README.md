# Pianoroller documentation.

Using pianoroller is simple:

* Plug in a USB MIDI keyboard
* Visit [the site](https://nasfarley88.gitlab.io/pianoroller/) in Google Chrome (or any browser that supports the [Web MIDI API](https://developer.mozilla.org/en-US/docs/Web/API/MIDIAccess))
* Start playing!

## Configuring a theme

To configure the theme, press the arrow on the side to open the sidebar. Use the colour pickers to pick the foreground and background colours.

For more advanced features of the theme, press 'Download Theme' button to download the theme as JSON. Then edit the JSON with your favourite text editor (_not_ Notepad). The theme currently has the following fields:

```json
{
  "author": "Nathanael Farley",
  "backgroundColor": "#000000bb",
  "blockColor": "#FF1690",
  "cssBackground": "black url(https://source.unsplash.com/collection/256789/1920x1080) 100% 100%",
  "experimental": {
    "enableClouds": false
  },
  "name": "Default theme"
}
```

* `author` is the author of the theme (does not affect display)
* `backgroundColor` sets the background of the blocks
* `blockColor` sets the color of the blocks that appear when notes are pressed
* `cssBackground` sets the CSS background attribute of the page (the default is a random picture from a collection of abstract photos from Unsplash stretched across the whole page)
* ` experimental` holds experimental features
* `name` is the name of the theme (does not affect display)